async function register ({
  registerSetting,
}) {
  registerSetting({
    type: 'input-checkbox',
    name: 'isEnabled',
    label: 'Enable iframe overlay',
    default: false,
    private: false
  });

  registerSetting({
    type: 'input',
    name: 'iframeUrl',
    label: 'URL to be loaded within iframe',
    default: '',
    private: false
  });

  registerSetting({
    type: 'input',
    name: 'buttonLabel',
    label: 'Label for button to hide modal',
    default: 'Close',
    private: false
  });
}

async function unregister () {
  return
}

module.exports = {
  register,
  unregister
}
