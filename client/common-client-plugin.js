const oneDayInMs = 3600 * 24 * 1000;

const addDaysToDate = (date, days) => {

  return new Date(date.getTime() + oneDayInMs * days);
};

const lessThanOneWeek = (dateString) => {
  const checkDate = new Date(dateString);

  if ((new Date() - checkDate) < oneDayInMs) {
    return true;
  }

  return false;
};

function register ({ registerHook, peertubeHelpers }) {
  peertubeHelpers.getSettings()
  .then(async (settings) => {
    if (!settings.isEnabled) return;

    const localStorageKey = 'donor-overlay';
    const lastShown = localStorage.getItem(localStorageKey);

    if (lastShown && lessThanOneWeek(lastShown)) {
      return;
    }

    const wrapper = document.createElement('div');
    const container = document.createElement('div');
    const id = 'peertube-plugin-iframe-overlay--container';
    container.classList.add(id);
    wrapper.appendChild(container);

    const elementToObserve = document.querySelector('body');

    const observer = new MutationObserver((records, obs) => {
      if (records.find((r) => r.target.classList.contains('modal-open'))) {
        obs.disconnect();
        setTimeout(() => {
          const footerHeight = document.querySelector('.modal-dialog .modal-footer').clientHeight;
          const iframe = document.createElement('iframe');
          iframe.src = settings.iframeUrl;
          iframe.style.height = `calc(100vh - (var(--bs-modal-margin) * 2) - (var(--bs-modal-header-padding-y) * 4) - (var(--bs-modal-padding) * 2) - ${footerHeight}px)`;
          iframe.style.width = 'calc(var(--bs-modal-width) - (var(--bs-modal-padding) * 2))';
          iframe.style.maxWidth = '100%';

          const c = document.querySelector(`.${id}`);

          c.appendChild(iframe);

          localStorage.setItem(localStorageKey, addDaysToDate(new Date(), 1).toISOString());
        });
      }
    });

    observer.observe(elementToObserve, { attributeFilter: ['class'] });

    peertubeHelpers.showModal({
      confirm: {
        value: settings.buttonLabel,
        action: () => {
          localStorage.setItem(localStorageKey, addDaysToDate(new Date(), 14).toISOString());
        },
      },
      content: wrapper.innerHTML,
      close: true,
    })
  })
}

export {
  register
}
